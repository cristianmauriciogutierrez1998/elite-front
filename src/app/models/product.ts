export interface Product {
  id?: number;
  ASIN: string;
  SKU: string;
  nombre: string;
  manual_usuario: string;
  precio: number;
  cantidad_disponible: number;
  descripcion: string;
  imagen: string;
}