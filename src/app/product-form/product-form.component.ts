import { Component, OnInit } from '@angular/core';
import { Product } from '../models/product';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit{

  productForm: FormGroup;
  
  product:Product = {} as Product;

  constructor(private formBuilder: FormBuilder, private http: HttpClient) { }

  ngOnInit(): void {
    this.productForm = this.formBuilder.group({
      asin: ['', Validators.required],
      sku: ['', Validators.required],
      nombre: ['', Validators.required],
      manual_usuario: ['', ],
      precio: ['', [Validators.required, Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      cantidad_disponible: ['',[Validators.required, Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      descripcion: ['', Validators.required],
      imagen: ['', Validators.required],
    });
  }

  onSubmit() {
    if (this.productForm.valid) {
      this.http.post('http://localhost:9000/api/products', this.productForm.value).subscribe(() => {
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Producto creado exitosamente',
          showConfirmButton: false,
          timer: 1500
        })
      }, error => {
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'No se pudo crear el producto',
          showConfirmButton: false,
          timer: 1500
        })
      });
    }
  }

}
